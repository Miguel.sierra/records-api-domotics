// Create express app
var express = require("express")
var app = express()
var mysql      = require('mysql');
var db = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'domotics'
});var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
// Server port
var HTTP_PORT = 4000 
// Start server
app.listen(HTTP_PORT, () => {
    console.log("Server running on port %PORT%".replace("%PORT%",HTTP_PORT))
});
// Root endpoint
app.get("/", (req, res, next) => {
    res.json({"message":"Ok"})
});

//app endpoint routes
app.get("/v1/records", (req, res, next) => {
    var sql = "select * from records"
    var params = []
    db.query(sql, (err, rows,fields) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":rows,
        })
      });
});


//by ID
app.get("/v1/records/:id", (req, res, next) => {
    var sql = "select * from records where id = ?"
    var params = [req.params.id]
    db.query(sql, params, (err, row) => {
        if (err) {
          res.status(400).json({"error":err.message});
          return;
        }
        res.json({
            "message":"success",
            "data":row
        })
      });
});
// "Content-Type": "application/x-www-form-urlencoded"
//save record
app.post("/v1/records/", (req, res, next) => {
    var errors=[]
    if (!req.body.url){
        errors.push("No url specified");
    }
    if (errors.length){
        res.status(400).json({"error":errors.join(",")});
        return;
    }
    var data = {
        description: req.body.description,
        url: req.body.url,
        recorded_at : req.body.recorded_at
    }
    var sql ='INSERT INTO records (description, url, recorded_at) VALUES (?,?,?)'
    var params =[data.description, data.url, data.recorded_at]
    db.query(sql, params, function (err, result) {
        if (err){
            res.status(400).json({"error": err.message})
            return;
        }
        res.json({
            "message": "success",
            "data": data,
            "id" : this.lastID
        })
    });
})

//delete record
app.delete("/v1/records/:id", (req, res, next) => {
    db.query(
        'DELETE FROM records WHERE id = ?',
        req.params.id,
        function (err, result) {
            if (err){
                res.status(400).json({"error": res.message})
                return;
            }
            res.json({"message":"deleted", changes: this.changes})
    });
})

// Insert here other API endpoints

// Default response for any other request
app.use(function(req, res){
    res.status(404);
});

